package com.webcoding.tutorials.model

data class TravelItemData(
    val name: String,
    val description: String,
    val imageResId: Int
)
