package com.webcoding.tutorials.model

import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.ui.text.input.KeyboardType

enum class InputTypeEnum(val options: KeyboardOptions) {
    TEXT(options = KeyboardOptions.Default),
    EMAIL(options = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Email)),
    PASSWORD(options = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Password)),
    TELEPHONE(options = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Phone)),
    DECIMAL(options = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Decimal)),
}