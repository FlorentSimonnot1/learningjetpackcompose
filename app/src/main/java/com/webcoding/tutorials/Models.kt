package com.webcoding.tutorials

import com.webcoding.tutorials.base.GViewModel

sealed interface ViewEvent : GViewModel.GEvent {
    object OnBoardingStatus : ViewEvent
}

internal interface ViewEffect : GViewModel.GSideEffect

internal data class ViewState(
    val isOnBoardingShown : Boolean? = null
) : GViewModel.GViewState


sealed interface ViewResult : GViewModel.GViewResult {
    data class OnBoardingShown(val isShown: Boolean) : ViewResult
}
