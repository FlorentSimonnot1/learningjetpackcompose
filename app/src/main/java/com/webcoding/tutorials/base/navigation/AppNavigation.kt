package com.webcoding.tutorials.base.navigation

import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.webcoding.tutorials.ui.features.listview.ListPageView
import com.webcoding.tutorials.ui.features.login.screen.LoginPageView
import com.webcoding.tutorials.ui.features.signin.SignInPageView
import com.webcoding.tutorials.ui.features.welcome.WelcomePageView

@Composable
fun AppNavigation(
    navController: NavHostController,
    startDestination: String,
) {
    NavHost(
        navController = navController,
        startDestination = startDestination,
    ) {
        welcomeScreen(navController)
        loginScreen(navController)
        signInScreen(navController)
        listViewScreen(navController)
    }
}

fun NavGraphBuilder.welcomeScreen(
    navController: NavController,
) {
    composable(
        route = Navigate.Screen.WelcomeScreen.route
    ) {
        val activity = (LocalContext.current as? Activity)

        BackHandler(true) {
            activity?.finish()
        }

        WelcomePageView(
            navController = navController,
            bottomBarPadding = PaddingValues()
        )
    }
}

fun NavGraphBuilder.loginScreen(
    navController: NavController
) {
    composable(
        route = Navigate.Screen.LoginScreen.route
    ) {
        LoginPageView(
            navController = navController,
            bottomBarPadding = PaddingValues()
        )
    }
}

fun NavGraphBuilder.signInScreen(
    navController: NavController
) {
    composable(
        route = Navigate.Screen.RegistrationScreen.route
    ) {
        SignInPageView(
            navController = navController,
            bottomBarPadding = PaddingValues()
        )
    }
}

fun NavGraphBuilder.listViewScreen(
    navController: NavController
) {
    composable(
        route = Navigate.Screen.ListViewScreen.route
    ) {
        ListPageView(
            navController = navController,
            bottomBarPadding = PaddingValues()
        )
    }
}