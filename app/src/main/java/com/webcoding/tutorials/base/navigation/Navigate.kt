package com.webcoding.tutorials.base.navigation

sealed class Navigate(val route: String) {

    sealed class Screen {
        object WelcomeScreen: Navigate("welcome")
        object LoginScreen: Navigate("login")
        object RegistrationScreen: Navigate("registration")
        object ListViewScreen: Navigate("listView")
    }
}