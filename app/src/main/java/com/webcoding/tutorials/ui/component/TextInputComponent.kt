package com.webcoding.tutorials.ui.component

import android.util.Log
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.webcoding.tutorials.model.InputTypeEnum
import com.webcoding.tutorials.ui.theme.Yellow

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TextInputComponent(
    value: String,
    placeholder: String,
    onChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    type: InputTypeEnum = InputTypeEnum.TEXT,
    paddingLeadingIconEnd: Dp = 0.dp,
    paddingTrailingIconStart: Dp = 0.dp,
    leadingIcon: (@Composable() () -> Unit)? = null,
    trailingIcon: (@Composable() () -> Unit)? = null,
    isInvalid: Boolean = false
) {
    val state = remember { mutableStateOf(TextFieldValue(value)) }

    var colors = TextFieldDefaults.outlinedTextFieldColors(
        cursorColor = Yellow,
        textColor = Yellow,
        unfocusedBorderColor = Yellow,
        focusedBorderColor = Yellow
    )

    if (isInvalid) {
        colors = TextFieldDefaults.outlinedTextFieldColors(
            cursorColor = Color.Red,
            textColor = Color.Red,
            unfocusedBorderColor = Color.Red,
            focusedBorderColor = Color.Red
        )
    }

    OutlinedTextField(
        value = state.value,
        onValueChange = {
            state.value = it
            onChange(it.text)
        },
        leadingIcon = leadingIcon,
        trailingIcon = trailingIcon,
        placeholder = { Text(text = placeholder, color = Yellow) },
        singleLine = true,
        shape = RoundedCornerShape(16.dp),
        colors = colors,
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 0.dp, vertical = 8.dp),
        keyboardOptions = type.options,
        visualTransformation = if (type == InputTypeEnum.PASSWORD)
                PasswordVisualTransformation()
            else VisualTransformation.None
    )
}