package com.webcoding.tutorials.ui.features.login.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.webcoding.tutorials.ui.features.login.data.LoginFormData
import com.webcoding.tutorials.utils.isValidEmail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
internal class LoginPageViewModel @Inject constructor() : ViewModel() {

    private val _uiState = MutableStateFlow(LoginFormData())
    val uiState: StateFlow<LoginFormData> = _uiState.asStateFlow()

    fun updateEmail(email: String) {
        viewModelScope.launch {
            _uiState.update { currentState ->
                val updatedEmailErrors = mutableListOf<String>()

                if (email.isEmpty()) {
                    updatedEmailErrors.add("Email is required")
                }

                if (!isValidEmail(email)) {
                    updatedEmailErrors.add("This is not a valid email")
                }

                currentState.copy(
                    email = email,
                    emailErrors = updatedEmailErrors
                )
            }
        }
    }

    fun updatePassword(password: String) {
        _uiState.update { currentState ->
            val updatedPasswordErrors = mutableListOf<String>()

            if (password.isEmpty()) {
                updatedPasswordErrors.add("Password is required")
            }

            currentState.copy(
                password = password,
                passwordErrors = updatedPasswordErrors
            )
        }
    }

    fun login() {
        _uiState.update {
            it.copy(
                email = it.email,
                password = it.password,
                emailErrors = it.emailErrors,
                passwordErrors = it.passwordErrors,
                submitted = true
            )
        }
    }

}