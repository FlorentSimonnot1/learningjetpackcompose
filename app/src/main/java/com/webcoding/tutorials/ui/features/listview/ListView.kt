package com.webcoding.tutorials.ui.features.listview

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.webcoding.tutorials.R
import com.webcoding.tutorials.model.TravelItemData
import com.webcoding.tutorials.ui.features.welcome.WelcomePageViewModel
import com.webcoding.tutorials.ui.theme.Blue
import com.webcoding.tutorials.ui.theme.Yellow
import com.webcoding.tutorials.ui.theme.Yellow25

@Composable
fun ListPageView(
    navController: NavController,
    bottomBarPadding: PaddingValues
) {
    ListPageViewComponent(
        viewModel = hiltViewModel(),
        navController = navController,
        bottomBarPadding = bottomBarPadding
    )
}

@Composable
internal fun ListPageViewComponent(
    viewModel: WelcomePageViewModel,
    navController: NavController,
    bottomBarPadding: PaddingValues
) {

    Box(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
            .padding(bottomBarPadding)
            .background(Yellow25)
    ) {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .wrapContentSize(Alignment.Center)
                .padding(28.dp)
        ) {

            // Title
            Text(
                text = "Choose your destination type",
                modifier = Modifier.padding(8.dp),
                style = TextStyle(color = Blue, fontWeight = FontWeight.Bold, fontSize = 22.sp)
            )

            Spacer(modifier = Modifier.size(30.dp))

            // List
            val dataList = listOf(
                TravelItemData(name = "To the beach", description = "", imageResId = R.drawable.summer_holidays),
                TravelItemData(name = "On a road trip", description = "", imageResId = R.drawable.business_trip),
                TravelItemData(name = "At the mountain", description = "", imageResId = R.drawable.mountain),
                TravelItemData(name = "On a cruise", description = "", imageResId = R.drawable.cruise),
                TravelItemData(name = "To the beach", description = "", imageResId = R.drawable.summer_holidays),
                TravelItemData(name = "On a road trip", description = "", imageResId = R.drawable.business_trip),
                TravelItemData(name = "At the mountain", description = "", imageResId = R.drawable.mountain),
                TravelItemData(name = "On a cruise", description = "", imageResId = R.drawable.cruise),
                TravelItemData(name = "To the beach", description = "", imageResId = R.drawable.summer_holidays),
                TravelItemData(name = "On a road trip", description = "", imageResId = R.drawable.business_trip),
                TravelItemData(name = "At the mountain", description = "", imageResId = R.drawable.mountain),
                TravelItemData(name = "On a cruise", description = "", imageResId = R.drawable.cruise),
            )
            MyListView(data = dataList)
        }
    }
}

@Composable
fun MyListView(data: List<TravelItemData>) {
    LazyColumn {
        items(data) { item ->
            TravelItem(item = item)
        }
    }
}

@Composable
fun TravelItem(item: TravelItemData) {
    ElevatedCard(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier.padding(4.dp),
    ) {
        Row(
            Modifier
                .padding(16.dp).fillMaxHeight(),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(Modifier.weight(1f).padding(8.dp), horizontalAlignment = Alignment.Start) {
                Image(
                    painter = painterResource(item.imageResId),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .size(52.dp)
                )
            }

            Column(
                Modifier
                    .weight(1f)
                    .padding(8.dp)
                    .fillMaxHeight(),
                verticalArrangement = Arrangement.Center
            ) {
                Text(text = item.name, fontWeight = FontWeight.Bold, modifier = Modifier.fillMaxHeight())
            }
        }
    }
}