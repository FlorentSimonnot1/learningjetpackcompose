package com.webcoding.tutorials.ui.features.login.data

data class LoginFormData(
    var email: String = "",
    var emailErrors: MutableList<String> = mutableListOf(),
    var password: String = "",
    var passwordErrors: MutableList<String> = mutableListOf(),
    var submitted: Boolean = false
)
