package com.webcoding.tutorials.ui.features.login.screen

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.layout
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import com.webcoding.tutorials.R
import com.webcoding.tutorials.model.InputTypeEnum
import com.webcoding.tutorials.ui.component.ButtonComponent
import com.webcoding.tutorials.ui.component.TextInputComponent
import com.webcoding.tutorials.ui.component.ToasterComponent
import com.webcoding.tutorials.ui.features.login.viewmodel.LoginPageViewModel
import com.webcoding.tutorials.ui.theme.Blue
import com.webcoding.tutorials.ui.theme.Yellow
import kotlin.math.roundToInt


@Composable
fun LoginPageView(
    navController: NavController,
    bottomBarPadding: PaddingValues
) {
    LoginComponent(
        viewModel = hiltViewModel(),
        navController = navController,
        bottomBarPadding = bottomBarPadding
    )
}



@Composable
internal fun LoginComponent(
    viewModel: LoginPageViewModel,
    navController: NavController,
    bottomBarPadding: PaddingValues
) {

    val state by viewModel.uiState.collectAsStateWithLifecycle()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Yellow),
        contentAlignment = Alignment.TopStart,
    ) {

        if (state.submitted) {
            ToasterComponent(
                message = "Bonjour ${state.email}",
                actionLabel = "Fermer",
            )
        }

        Image(
            painter = painterResource(R.drawable.bg_korea),
            contentDescription = null,
            modifier = Modifier.fillMaxWidth()
        )

        Box(
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .layout { measurable, constraints ->
                    val height = constraints.maxHeight / 1.5
                    val placeable =
                        measurable.measure(constraints.copy(maxHeight = height.roundToInt()))
                    layout(placeable.width, height.roundToInt()) {
                        placeable.placeRelative(0, 0)
                    }
                }
                .fillMaxWidth()
                .clip(RoundedCornerShape(topStart = 28.dp, topEnd = 28.dp))
                .background(Color.White)
                .padding(28.dp)
        ) {
            Column(Modifier.fillMaxSize()) {

                Text(
                    text = "Bienvenue",
                    color = Blue,
                    style = TextStyle(fontWeight = FontWeight.Bold, fontSize = 22.sp)
                )

                Spacer(modifier = Modifier.size(30.dp))

                TextInputComponent(
                    value = state.email,
                    onChange = {
                        viewModel.updateEmail(it)
                    },
                    placeholder = "Username",
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 8.dp),
                    type = InputTypeEnum.EMAIL,
                    isInvalid = state.emailErrors.isNotEmpty()
                )

                TextInputComponent(
                    value = state.password,
                    onChange = {
                        viewModel.updatePassword(it)
                    },
                    placeholder = "Password",
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 8.dp),
                    type = InputTypeEnum.PASSWORD,
                    isInvalid = state.passwordErrors.isNotEmpty()
                )

                ButtonComponent(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(50.dp),
                    text = "Log in",
                    colors = ButtonDefaults.buttonColors(containerColor = Yellow),
                    borderStroke = BorderStroke(1.dp, Yellow),
                    textStyle = TextStyle(fontWeight = FontWeight.Bold, color = Color.White, fontSize = 22.sp)
                ) {
                    viewModel.login()
                }
            }
        }

        Box(modifier = Modifier
            .padding(28.dp)
            .align(Alignment.BottomCenter)) {
            ButtonComponent(
                modifier = Modifier
                    .fillMaxWidth(),
                text = "Create my account",
                colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
                textStyle = TextStyle(fontWeight = FontWeight.Bold, color = Yellow, fontSize = 16.sp)
            )
        }
    }

}