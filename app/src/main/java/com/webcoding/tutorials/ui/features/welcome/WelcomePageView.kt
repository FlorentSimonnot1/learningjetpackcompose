package com.webcoding.tutorials.ui.features.welcome

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.webcoding.tutorials.R
import com.webcoding.tutorials.base.navigation.Navigate
import com.webcoding.tutorials.ui.component.ButtonComponent
import com.webcoding.tutorials.ui.theme.Blue
import com.webcoding.tutorials.ui.theme.Yellow
import com.webcoding.tutorials.ui.theme.Yellow25


@Composable
fun WelcomePageView(
    navController: NavController,
    bottomBarPadding: PaddingValues
) {
    WelcomeComponent(
        viewModel = hiltViewModel(),
        navController = navController,
        bottomBarPadding = bottomBarPadding
    )
}


@Composable
internal fun WelcomeComponent(
    viewModel: WelcomePageViewModel,
    navController: NavController,
    bottomBarPadding: PaddingValues
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
            .padding(bottomBarPadding)
            .background(Yellow25)
    ) {
        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxSize().wrapContentSize(Alignment.Center).padding(28.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.img1),
                contentDescription = stringResource(R.string.explore_the_world)
            )

            Spacer(modifier = Modifier.size(30.dp))

            Text(
                text = stringResource(id = R.string.explore_the_world),
                modifier = Modifier.padding(bottom = 8.dp)
                    .fillMaxWidth(),
                style = TextStyle(fontSize = 32.sp, color = Blue, fontWeight = FontWeight.Bold),
                textAlign = TextAlign.Center
            )

            Spacer(modifier = Modifier.size(30.dp))

            Text(
                text = stringResource(id = R.string.welcome_subtitle),
                modifier = Modifier.padding(bottom = 8.dp)
                    .fillMaxWidth(),
                style = TextStyle(fontSize = 16.sp, color = Color.Black, fontWeight = FontWeight.Bold),
                textAlign = TextAlign.Center
            )

            Spacer(modifier = Modifier.size(30.dp))

            ButtonComponent(
                modifier = Modifier
                    .fillMaxWidth().height(50.dp),
                text = "Login",
                colors = ButtonDefaults.buttonColors(containerColor = Yellow),
                textStyle = TextStyle(fontWeight = FontWeight.Bold)
            ) {
                navController.navigate(Navigate.Screen.LoginScreen.route)
            }

            Spacer(modifier = Modifier.size(30.dp))

            ButtonComponent(
                modifier = Modifier
                    .fillMaxWidth().height(50.dp),
                text = "Create new account",
                colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
                borderStroke = BorderStroke(1.dp, Yellow),
                textStyle = TextStyle(fontWeight = FontWeight.Bold, color = Yellow)
            ) {
                navController.navigate(Navigate.Screen.RegistrationScreen.route)
            }

            Spacer(modifier = Modifier.size(30.dp))

            ButtonComponent(
                modifier = Modifier
                    .fillMaxWidth().height(50.dp),
                text = "Check the list",
                colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
                borderStroke = BorderStroke(1.dp, Yellow),
                textStyle = TextStyle(fontWeight = FontWeight.Bold, color = Yellow)
            ) {
                navController.navigate(Navigate.Screen.ListViewScreen.route)
            }
        }
    }
}