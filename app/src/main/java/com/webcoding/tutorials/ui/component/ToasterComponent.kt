package com.webcoding.tutorials.ui.component

import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember

@Composable
fun ToasterComponent(
    message: String,
    actionLabel: String? = null,
    action: (() -> Unit)? = null
) {
    val snackbarHostState = remember { SnackbarHostState() }

    LaunchedEffect(key1 = message) {
        snackbarHostState.showSnackbar(message)
    }

    SnackbarHost(
        hostState = snackbarHostState,
        snackbar = {
            Snackbar(
                action = {
                    action?.invoke()
                },
            ) {
                Text(text = message)
            }
        }
    )

}