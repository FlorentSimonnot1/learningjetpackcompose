package com.webcoding.tutorials

import androidx.lifecycle.ViewModel
import com.webcoding.tutorials.base.GViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
internal class MainViewModel @Inject constructor(): ViewModel()  {


}