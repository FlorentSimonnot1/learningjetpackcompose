package com.webcoding.tutorials

import androidx.compose.runtime.Composable
import androidx.navigation.compose.rememberNavController
import com.webcoding.tutorials.base.navigation.AppNavigation
import com.webcoding.tutorials.base.navigation.Navigate

@Composable
internal fun MainView(mainViewModel: MainViewModel, onLauncherFinished : () -> Unit) {

    val navController = rememberNavController()

    val screen = Navigate.Screen.WelcomeScreen.route

    AppNavigation(
        navController = navController,
        startDestination = screen,
    )


    onLauncherFinished()
}
